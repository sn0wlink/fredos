# Freds Post Debian KDE Installer

## NOTES ##
# Need to check support for app images (TODO)
# Add support for app images -> appimaged

# Begin installation

#Update the OS
sudo apt update && sudo apt upgrade -y

# Install Brave Browser (for ad-free web surfing)
sudo apt install apt-transport-https curl
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser -y

## Add flatpak support
sudo apt install flatpak -y
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo apt install plasma-discover-backend-flatpak -y

# Add AppImage window intergration (installs appimage launcher)
cd /tmp
wget https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/appimagelauncher_2.2.0-travis995.0f91801.bionic_amd64.deb
sudo dpkg -i appimagelauncher_2.2.0-travis995.0f91801.bionic_amd64.deb

# Apple Wifi Support (Macbook Air a1465) Broadcom Driver
sudo apt install firmware-b43-installer -y
sudo apt install broadcom-sta-dkms -y

# Core sysadmin tools
sudo apt install git -y # Adds support for Git Repos
sudo apt install gufw -y # Install Firewall GUI (Why the hell isn't this standard?)
sudo apt install thunderbird -y # Remove KMail and Install Thunderbird
sudo apt install remmina -y # Multiplatform Remote Client (RDP, SSH, VNC, etc)
sudo apt install htop -y
sudo apt install tmux -y
sudo apt install vlc -y
sudo apt install ffmpeg -y # Multimedia decoding support
sudo apt install ffmpegthumbs -y # Install video thumnail support for Dolphin file browser
sudo apt install putty -y
sudo apt install barrier # Keyboard and mouse sharing (Synergy alternative)
sudo apt install krfb -y # Remote VNC Server GUI
sudo apt install krdc -y # Remote VNC Client
sudo apt install xrdp -y # Install Microsoft RDP Support
sudo apt install wine -y # Windows application support
sudo apt-get install network-manager-l2tp -y # Add L2TP IPSEC VPN Protocol Support
sudo apt install tlp -y # Install TLP for power management
sudo apt install dmraid -y # Install software raid support
sudo apt install mdadm -y # Install software raid support


########################################################
#   My Personal Stuff (PRIVATE!!!!!!!!!)
########################################################

# Install Sublime Text + Sublime Merge
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text -y
sudo apt-get install sublime-merge -y

# Software and Flatpak installs
# sudo snap install bitwarden
# sudo snap install 1password
# sudo snap install wonderwall
# sudo snap install discord

sudo flatpak install flathub cc.arduino.IDE2 -y
sudo flatpak install flathub com.visualstudio.code -y
sudo flatpak install flathub com.microsoft.Edge -y
sudo flatpak install flathub md.obsidian.Obsidian -y
sudo flatpak install flathub com.behringer.XAirEdit -y
sudo flatpak install flathub com.github.rajsolai.response -y
sudo flatpak install flathub io.dbeaver.DBeaverCommunity -y
sudo flatpak install flathub com.google.AndroidStudio -y
sudo flatpak install flathub com.discordapp.Discord -y
sudo flatpak install flathub com.ktechpit.wonderwall -y
sudo flatpak install flathub tv.plex.PlexDesktop -y
sudo flatpak install flathub com.bitwarden.desktop -y
sudo flatpak install flathub com.valvesoftware.Steam -y

# Install 1Password
cd /tmp
sudo apt install gnupg2 -y
wget https://downloads.1password.com/linux/debian/amd64/stable/1password-latest.deb
sudo dpkg -i /tmp/1password-latest.deb
cd

# Install Termius
cd /tmp
wget https://www.termius.com/download/linux/Termius.deb
sudo dpkg -i Termius.deb
cd

sudo apt install inkscape -y
sudo apt install gimp -y
sudo apt install audacity -y
sudo apt install onedrive -y
sudo apt install darktable -y
sudo apt install openscad -y
sudo apt install cura -y
sudo apt install handbrake -y

# Starlabs Stuff
cd /tmp
wget https://github.com/StarLabsLtd/packages/raw/main/Debian/11/coreboot-configurator_10+f_all.deb \
	https://github.com/StarLabsLtd/packages/raw/main/Debian/11/nvramtool_1_all.deb
sudo dpkg -i *.deb
sudo apt -f install -y
