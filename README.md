# Fred OS

A script to install and setup a clean installation of your chosen OS. This should apply various fixes and patches and setup standard tools automagically for your system admin and entertainment needs. Usually this will also add the ability to make your system compatible with flatpaks, appimages, snaps etc, so you need not worry about whether it will work or not.

FredOS - Making life easier!

## Contribution Rules
- No kicking
- No biting
- No peeking

- And be excellent to each other!
