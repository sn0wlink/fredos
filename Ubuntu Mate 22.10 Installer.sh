# Freds Post Ubuntu Installer

#Update the OS
sudo apt update && sudo apt upgrade -y

# Add flatpak support
sudo apt install gnome-software -y
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Install Brave Browser
sudo apt install apt-transport-https curl
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser -y

# Remove firefox
sudo snap remove firefox

# Install TLP for power management
sudo apt install tlp -y

# Wine
sudo apt install wine -y

# Install and configure XRDP
sudo apt install xrdp -y
sudo sed -i '15 a export $(dbus-launch)' /etc/xrdp/startwm.sh
sudo systemctl enable xrdp
sudo systemctl start xrdp

# Fix Previous versions utility in caja
sudo apt install python3-pydrive -y

# Add L2TP VPN Protocol Support
sudo apt-get install network-manager-l2tp network-manager-l2tp-gnome -y

# Setup FredOS Wallpaper
# (Make sure this is in the same directory as the run script)
gsettings set org.mate.background picture-filename $PWD/wallpaper_1080.png

########################################################
#   My Personal Stuff (PRIVATE!!!!!!!!!)
########################################################

# Install Sublime Text + Sublime Merge
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text -y
sudo apt-get install sublime-merge -y

# Software and Flatpak installs
sudo snap install bitwarden
sudo snap install 1password
sudo snap install wonderwall
sudo snap install discord

flatpak install --noninteractive cc.arduino.IDE2
flatpak install --noninteractive com.visualstudio.code
flatpak install --noninteractive com.microsoft.Edge
flatpak install --noninteractive md.obsidian.Obsidian
flatpak install --noninteractive com.behringer.XAirEdit
flatpak install --noninteractive com.github.rajsolai.response
flatpak install --noninteractive io.dbeaver.DBeaverCommunity
flatpak install --noninteractive com.google.AndroidStudio

sudo apt install inkscape -y
sudo apt install gimp -y
sudo apt install audacity -y
sudo apt install onedrive -y
sudo apt install steam -y
sudo apt install putty -y
sudo apt install darktable -y
sudo apt install openscad -y
sudo apt install cura -y
sudo apt install handbrake -y

# Starlabs Stuff
sudo add-apt-repository ppa:starlabs/coreboot -y
sudo apt update
sudo apt install coreboot-configurator -y
